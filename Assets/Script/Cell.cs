﻿using UnityEngine;

namespace Script
{
    public class Cell : MonoBehaviour
    {
        [SerializeField] public bool isTrue;

        [SerializeField] public SpriteRenderer celling;

        [SerializeField] private Animator animator;

        private GameController _gameController;

        private void Start()
        {
            var gameControllerObject = GameObject.Find("GameController");
            _gameController = gameControllerObject.GetComponent<GameController>();
        }

        private void OnMouseDown()
        {
            Debug.Log(isTrue);
            if (isTrue)
            {
                _gameController.NextLevel();
            }
            else
            {
                animator.SetBool("isFalse", true);
            }
        }

        private void OnMouseUp()
        {
            animator.SetBool("isFalse", false);
        }
    }
}