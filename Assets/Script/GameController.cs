﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Script
{
    public class GameController : MonoBehaviour
    {
        [SerializeField] private Text questText;

        [SerializeField] private int level;

        [SerializeField] private GameObject gamePanel;

        [SerializeField] private GameObject finishPanel;

        [SerializeField] private GameObject cellPrefab;

        [SerializeField] private GameObject plaything;

        [SerializeField] private List<Cell> cellsList;

        [SerializeField] private List<Sprite> spriteList;

        [SerializeField] private List<Quest> questList;

        public void SceneStart()
        {
            level = 1;
            ClearField();
            SpawnCells();
            SetQuest();
        }

        public void NextLevel()
        {
            if (level == 3)
            {
                FinishLevel();
                return;
            }

            Debug.Log("NextLVL");
            level++;
            ClearField();
            SpawnCells();
            SetQuest();
        }

        private void FinishLevel()
        {
            ClearField();
            finishPanel.SetActive(true);
            gamePanel.SetActive(false);
        }

        private void ClearField()
        {
            foreach (var cell in cellsList)
            {
                Destroy(cell.gameObject);
            }

            cellsList.Clear();
            questText.text = "";
        }

        private void SetQuest()
        {
            var isSetCell = false;
            var randomQuestIndex = Random.Range(0, questList.Count);
            var quest = questList[randomQuestIndex];
            questText.text = quest.text;
            foreach (var cell in cellsList)
            {
                if (cell.celling.sprite.Equals(quest.answer))
                {
                    cell.isTrue = true;
                    isSetCell = true;
                }
            }

            if (!isSetCell)
            {
                var randomCellIndex = Random.Range(0, cellsList.Count);
                cellsList[randomCellIndex].celling.sprite = quest.answer;
                cellsList[randomCellIndex].isTrue = true;
            }
        }

        private void SpawnCells()
        {
            for (var i = 0; i < spriteList.Count; i++)
            {
                var randomSpriteIndex = Random.Range(0, spriteList.Count);
                var newSprite = spriteList[i];
                spriteList[i] = spriteList[randomSpriteIndex];
                spriteList[randomSpriteIndex] = newSprite;
            }

            for (var i = 0; i < level * 3; i++)
            {
                var newCell = Instantiate(cellPrefab, plaything.transform).GetComponent<Cell>();
                cellsList.Add(newCell);
                newCell.celling.sprite = spriteList[i];
            }
        }
    }
}