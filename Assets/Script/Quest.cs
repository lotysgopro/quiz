﻿using UnityEngine;

namespace Script
{
    [System.Serializable]
    public class Quest
    {
        public string text;
        public Sprite answer;
    }
}